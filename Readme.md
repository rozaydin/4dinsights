# NON PKCS 12 Format RSA Key   
    
keytool -genkey -alias <desired certificate alias> 
    -keystore <path to keystore.pfx>
     # -storetype PKCS12 
    -keyalg RSA 
    -storepass password 
    -validity 730 
    -keysize 2048 
	
	
# Contents Of the jks file

keytool -list -v -keystore jwt.jks
Enter keystore password:
Keystore type: jks
Keystore provider: SUN

Your keystore contains 1 entry

Alias name: signedurl
Creation date: Feb 10, 2020
Entry type: PrivateKeyEntry
Certificate chain length: 1
Certificate[1]:
Owner: CN=Ridvan Ozaydin, OU=Software Development, O=freelance, L=Istanbul, ST=Istanbul, C=TR
Issuer: CN=Ridvan Ozaydin, OU=Software Development, O=freelance, L=Istanbul, ST=Istanbul, C=TR
Serial number: a441ca1
Valid from: Mon Feb 10 11:15:03 EET 2020 until: Wed Dec 19 11:15:03 EET 2029
Certificate fingerprints:
         MD5:  B0:B4:AA:58:9F:1E:8F:30:06:14:4D:BB:0B:91:A4:F7
         SHA1: 93:C2:44:83:26:1C:3B:4B:24:53:03:7F:0C:20:71:87:21:66:CC:EF
         SHA256: CE:71:CA:77:BE:7E:7C:35:58:BD:46:33:D8:B6:B8:7F:C7:C2:1F:22:93:59:1F:A9:CE:27:44:F3:FF:D3:47:EC
Signature algorithm name: SHA256withRSA
Subject Public Key Algorithm: 2048-bit RSA key
Version: 3

Extensions:

#1: ObjectId: 2.5.29.14 Criticality=false
SubjectKeyIdentifier [
KeyIdentifier [
0000: 72 8F C4 FF AA DA 00 F9   BF EA 27 EE E0 EC EB 7C  r.........'.....
0010: 37 90 BF F9                                        7...
]
]



*******************************************
*******************************************



Warning:
The JKS keystore uses a proprietary format. It is recommended to migrate to PKCS12 which is an industry standard format using "keytool -importkeystore -srckeystore jwt.jks -destkeystore jwt.jks -deststoretype pkcs12".