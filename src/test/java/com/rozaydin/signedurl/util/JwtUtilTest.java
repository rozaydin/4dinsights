package com.rozaydin.signedurl.util;

import com.rozaydin.signedurl.config.Config;
import com.rozaydin.signedurl.config.RSAKeyConfig;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtContext;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.security.rsa.crypto.KeyStoreKeyFactory;

import java.security.PrivateKey;
import java.security.PublicKey;

import static com.rozaydin.signedurl.util.Constants.PASSWORD_RESET_SCOPE;
import static org.assertj.core.api.Assertions.assertThat;

public class JwtUtilTest {

    private static String issuer = "signedurl";
    private static String subject = "userId";
    private static String jti = "test jti";
    private static String[] scope = new String[]{PASSWORD_RESET_SCOPE};
    private static String keyId = "signedurl";
    private static long validityPeriodInSec = 7200;
    //
    private static PrivateKey privateKey;
    private static PublicKey publicKey;

    @BeforeClass
    public static void init() {
        KeyStoreKeyFactory keyStoreKeyFactory = new RSAKeyConfig("jwt.jks", "password".toCharArray()).getKeyStoreFactory();
        privateKey = keyStoreKeyFactory.getKeyPair(keyId).getPrivate();
        publicKey = keyStoreKeyFactory.getKeyPair(keyId).getPublic();
    }

    @Test
    public void should_generate_valid_jwt_using_provided_parameters() throws Exception {

        // GIVEN
        String jwt = JwtUtil.createJwt(issuer, subject, validityPeriodInSec, jti, scope, keyId, privateKey);
        JwtConsumer consumer = new Config().jwtValidator(publicKey, issuer);
        // WHEN
        JwtContext jwtContext = consumer.process(jwt);
        // THEN
        assertThat(jwtContext.getJwtClaims().getSubject()).isEqualTo(subject);
        assertThat(jwtContext.getJwtClaims().getJwtId()).isEqualTo(jti);
        assertThat(jwtContext.getJwtClaims().getExpirationTime().getValue()).isEqualTo(jwtContext.getJwtClaims().getIssuedAt().getValue() + validityPeriodInSec);
    }

}
