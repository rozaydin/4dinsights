package com.rozaydin.signedurl.service;

import com.rozaydin.signedurl.entity.UserEntity;
import com.rozaydin.signedurl.exceptions.UserNotFoundException;
import com.rozaydin.signedurl.repository.UserRepository;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

public class UserServiceTest {

    @Test
    public void should_login_existing_users_with_correct_password() {

        // GIVEN
        final long userId = 15;
        //
        UserEntity userEntity = new UserEntity();
        userEntity.setId(userId);
        userEntity.setPassword("password");
        userEntity.setUsername("test_user");
        userEntity.setJti("test_jti");
        //
        UserRepository userRepository = Mockito.mock(UserRepository.class);
        Mockito.when(userRepository.findById(userId)).thenReturn(Optional.of(userEntity));
        Mockito.when(userRepository.save(userEntity)).thenReturn(userEntity);
        //
        UserService userService = new UserService(userRepository);

        // WHEN
        boolean loginResult = userService.login(userId, "password");
        // THEN
        assertThat(loginResult).isTrue();
    }

    @Test
    public void should_not_login_existing_users_with_incorrect_password() {

        // GIVEN
        final long userId = 15;
        //
        UserEntity userEntity = new UserEntity();
        userEntity.setId(userId);
        userEntity.setPassword("password");
        userEntity.setUsername("test_user");
        userEntity.setJti("test_jti");
        //
        UserRepository userRepository = Mockito.mock(UserRepository.class);
        Mockito.when(userRepository.findById(userId)).thenReturn(Optional.of(userEntity));
        Mockito.when(userRepository.save(userEntity)).thenReturn(userEntity);
        //
        UserService userService = new UserService(userRepository);

        // WHEN
        boolean loginResult = userService.login(userId, "wrong password");
        // THEN
        assertThat(loginResult).isFalse();
    }

    @Test(expected = UserNotFoundException.class)
    public void should_throw_exception_when_non_existent_users_try_login() {

        // GIVEN
        final long userId = 15;
        //
        UserRepository userRepository = Mockito.mock(UserRepository.class);
        Mockito.when(userRepository.findById(userId)).thenReturn(Optional.empty());
        //
        UserService userService = new UserService(userRepository);
        // WHEN
        // THEN
        userService.login(userId, "password");
    }

    @Test
    public void should_update_jti_for_valid_users() {

        // GIVEN
        final long userId = 15;
        //
        UserEntity userEntity = new UserEntity();
        userEntity.setId(userId);
        userEntity.setPassword("password");
        userEntity.setUsername("test_user");
        userEntity.setJti("test_jti");
        //
        UserRepository userRepository = Mockito.mock(UserRepository.class);
        Mockito.when(userRepository.findById(userId)).thenReturn(Optional.of(userEntity));
        Mockito.when(userRepository.save(userEntity)).thenReturn(userEntity);
        //
        UserService userService = new UserService(userRepository);

        // WHEN
        final String updatedJti = userService.updateJti(userId);

        // THEN
        assertThat(updatedJti).isEqualTo(userEntity.getJti());
        assertThat(updatedJti).isNotEqualTo("test_jti");
        Mockito.verify(userRepository, Mockito.times(1)).findById(userId);
        Mockito.verify(userRepository, Mockito.times(1)).save(userEntity);
    }

    @Test(expected = UserNotFoundException.class)
    public void should_throw_exception_when_requested_to_update_jti_for_non_existent_users() {

        // GIVEN
        final long userId = 15;
        UserRepository userRepository = Mockito.mock(UserRepository.class);
        Mockito.when(userRepository.findById(userId)).thenReturn(Optional.empty());
        UserService userService = new UserService(userRepository);

        // WHEN
        // THEN

        final String updatedJti = userService.updateJti(userId);

    }

    @Test
    public void should_reset_password_for_valid_users() {

        // GIVEN
        final long userId = 15;
        //
        UserEntity userEntity = new UserEntity();
        userEntity.setId(userId);
        userEntity.setPassword("password");
        userEntity.setUsername("test_user");
        userEntity.setJti("test_jti");
        //
        UserRepository userRepository = Mockito.mock(UserRepository.class);
        Mockito.when(userRepository.findById(userId)).thenReturn(Optional.of(userEntity));
        Mockito.when(userRepository.save(userEntity)).thenReturn(userEntity);
        //
        UserService userService = new UserService(userRepository);

        // WHEN
        userService.resetPassword(userId, "newPassword");

        // THEN
        assertThat(userEntity.getPassword()).isEqualTo("newPassword");
        Mockito.verify(userRepository, Mockito.times(1)).findById(userId);
        Mockito.verify(userRepository, Mockito.times(1)).save(userEntity);
    }

    @Test(expected = UserNotFoundException.class)
    public void should_throw_exception_when_requested_to_reset_password_for_non_existent_users() {

        // GIVEN
        final long userId = 15;
        UserRepository userRepository = Mockito.mock(UserRepository.class);
        Mockito.when(userRepository.findById(userId)).thenReturn(Optional.empty());
        //
        UserService userService = new UserService(userRepository);

        // WHEN
        // THEN
        userService.resetPassword(userId, "newPassword");

    }


}
