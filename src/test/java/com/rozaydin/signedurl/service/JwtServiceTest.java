package com.rozaydin.signedurl.service;

import com.rozaydin.signedurl.config.Config;
import com.rozaydin.signedurl.config.RSAKeyConfig;
import com.rozaydin.signedurl.entity.UserEntity;
import com.rozaydin.signedurl.repository.UserRepository;
import org.jose4j.jwt.NumericDate;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtContext;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.security.rsa.crypto.KeyStoreKeyFactory;

import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

public class JwtServiceTest {

    private static String issuer = "signedurl";
    private static String keyId = "signedurl";
    private static long validityPeriodInSec = 7200;
    //
    private static PrivateKey privateKey;
    private static PublicKey publicKey;

    @BeforeClass
    public static void init() {
        KeyStoreKeyFactory keyStoreKeyFactory = new RSAKeyConfig("jwt.jks", "password".toCharArray()).getKeyStoreFactory();
        privateKey = keyStoreKeyFactory.getKeyPair(keyId).getPrivate();
        publicKey = keyStoreKeyFactory.getKeyPair(keyId).getPublic();
    }

    @Test
    public void jwtService_should_create_valid_jwt() throws Exception {

        // GIVEN
        final long userId = 15;
        //
        UserEntity userEntity = new UserEntity();
        UserRepository userRepository = Mockito.mock(UserRepository.class);
        Mockito.when(userRepository.findById(userId)).thenReturn(Optional.of(userEntity));
        //
        JwtConsumer jwtConsumer = new Config().jwtValidator(publicKey, issuer);
        //
        JwtService service = new JwtService(privateKey, issuer, keyId, validityPeriodInSec, jwtConsumer, userRepository);

        // WHEN
        final String jwt = service.createJwt(15, "test-jti");

        // THEN
        JwtConsumer consumer = new Config().jwtValidator(publicKey, issuer);
        JwtContext jwtContext = consumer.process(jwt);

        NumericDate iat = jwtContext.getJwtClaims().getIssuedAt();
        NumericDate nbf = jwtContext.getJwtClaims().getNotBefore();
        NumericDate exp = jwtContext.getJwtClaims().getExpirationTime();

        assertThat(iat).isEqualTo(nbf);
        assertThat(iat.getValueInMillis()).isLessThanOrEqualTo(System.currentTimeMillis());
        assertThat(exp.getValueInMillis()).isGreaterThan(System.currentTimeMillis());
        assertThat(exp.isAfter(iat)).isTrue();
        assertThat(exp.getValue() - iat.getValue()).isEqualTo(validityPeriodInSec);
        assertThat(jwtContext.getJwtClaims().getSubject()).isEqualTo(String.valueOf(userId));
        assertThat(jwtContext.getJwtClaims().getJwtId()).isEqualTo("test-jti");
    }

    @Test
    public void jwtService_should_validate_valid_jwts() throws Exception {

        // GIVEN
        final String jtiId = "test-jti-id";
        final long userId = 15;
        //
        UserEntity userEntity = new UserEntity();
        userEntity.setId(userId);
        userEntity.setJti(jtiId);
        UserRepository userRepository = Mockito.mock(UserRepository.class);
        Mockito.when(userRepository.findById(userId)).thenReturn(Optional.of(userEntity));
        //
        JwtConsumer jwtConsumer = new Config().jwtValidator(publicKey, issuer);
        //
        JwtService service = new JwtService(privateKey, issuer, keyId, validityPeriodInSec, jwtConsumer, userRepository);

        // WHEN
        final String jwt = service.createJwt(15, jtiId);

        // THEN
        assertThat(service.validateJwt(userId, jwt)).isTrue();
    }

    @Test
    public void jwtService_should_not_validate_expired_jwts() throws Exception {

        // GIVEN
        final String jtiId = "test-jti-id";
        final long userId = 15;
        //
        UserEntity userEntity = new UserEntity();
        userEntity.setId(userId);
        userEntity.setJti(jtiId);
        UserRepository userRepository = Mockito.mock(UserRepository.class);
        Mockito.when(userRepository.findById(userId)).thenReturn(Optional.of(userEntity));
        //
        JwtConsumer jwtConsumer = new Config().jwtValidator(publicKey, issuer);
        // already expired token
        JwtService service = new JwtService(privateKey, issuer, keyId, -1, jwtConsumer, userRepository);

        // WHEN
        final String jwt = service.createJwt(15, jtiId);

        // THEN
        assertThat(service.validateJwt(userId, jwt)).isFalse();

    }

    @Test
    public void jwtService_should_not_validate_valid_jwts_when_jti_does_not_match() throws Exception {

        // GIVEN
        final String jtiId = "test-jti-id";
        final long userId = 15;
        //
        UserEntity userEntity = new UserEntity();
        userEntity.setId(userId);
        userEntity.setJti("test-invalid-jti-id");
        UserRepository userRepository = Mockito.mock(UserRepository.class);
        Mockito.when(userRepository.findById(userId)).thenReturn(Optional.of(userEntity));
        //
        JwtConsumer jwtConsumer = new Config().jwtValidator(publicKey, issuer);
        //
        JwtService service = new JwtService(privateKey, issuer, keyId, validityPeriodInSec, jwtConsumer, userRepository);

        // WHEN
        final String jwt = service.createJwt(15, jtiId);

        // THEN
        assertThat(service.validateJwt(userId, jwt)).isFalse();
    }

}