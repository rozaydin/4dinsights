package com.rozaydin.signedurl.config;

import com.rozaydin.signedurl.repository.UserRepository;
import com.rozaydin.signedurl.service.JwtService;
import com.rozaydin.signedurl.util.ValidatorUtil;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;
import org.jose4j.jwt.consumer.Validator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.security.PublicKey;

import static com.rozaydin.signedurl.util.Constants.PASSWORD_RESET_SCOPE;
import static com.rozaydin.signedurl.util.Constants.SCOPE_CLAIM;

@Configuration
public class Config {

    @Bean
    public JwtConsumer jwtValidator(PublicKey publicKey, @Value("${jwt.issuer}") String issuer) {

        // Scope Claim Validator
        Validator scopeClaimValidator =
                ValidatorUtil.createJose4JExpectedClaimValidator(
                        SCOPE_CLAIM, new String[]{PASSWORD_RESET_SCOPE});

        return new JwtConsumerBuilder()
                .setVerificationKey(publicKey)
                .setExpectedIssuer(issuer)
                // set scope claim validator
                .registerValidator(scopeClaimValidator)
                .build();
    }

}
