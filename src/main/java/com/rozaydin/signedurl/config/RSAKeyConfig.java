package com.rozaydin.signedurl.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.rsa.crypto.KeyStoreKeyFactory;

import java.security.PrivateKey;
import java.security.PublicKey;

@Configuration
public class RSAKeyConfig {

    private final String keystorePath;
    private final char[] keystorePassword;

    public RSAKeyConfig(@Value("${security.keystore.path}") String keystorePath,
                        @Value("${security.keystore.password}") char[] keystorePassword) {
        this.keystorePath = keystorePath;
        this.keystorePassword = keystorePassword;
    }

    @Bean
    public KeyStoreKeyFactory getKeyStoreFactory() {
        return new KeyStoreKeyFactory(new ClassPathResource(keystorePath), keystorePassword);
    }

    @Bean
    public PublicKey getPublicKey(KeyStoreKeyFactory keyStoreKeyFactory, @Value("${security.keystore.keypair.alias}") String alias) {
        return keyStoreKeyFactory.getKeyPair(alias).getPublic();
    }

    @Bean
    public PrivateKey getPrivateKey(KeyStoreKeyFactory keyStoreKeyFactory, @Value("${security.keystore.keypair.alias}") String alias) {
        return keyStoreKeyFactory.getKeyPair(alias).getPrivate();
    }


}