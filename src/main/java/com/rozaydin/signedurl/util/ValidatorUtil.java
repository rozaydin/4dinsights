package com.rozaydin.signedurl.util;

import org.jose4j.jwt.MalformedClaimException;
import org.jose4j.jwt.consumer.JwtContext;
import org.jose4j.jwt.consumer.Validator;

import java.util.Arrays;
import java.util.List;

public class ValidatorUtil {

    public static Validator createJose4JExpectedClaimValidator(
            final String claim, final String value) {

        return new Validator() {
            @Override
            public String validate(JwtContext jwtContext) throws MalformedClaimException {

                if (jwtContext.getJwtClaims().hasClaim(claim)) {
                    String claimValue = jwtContext.getJwtClaims().getStringClaimValue(claim);
                    return value.equals(claimValue)
                            ? null
                            : String.format(
                            "claim %s value: %s does not match value present in JWT: %s",
                            claim, value, claimValue);

                } else {
                    return String.format("JWT does not have %s claim", claim);
                }
            }
        };
    }

    public static Validator createJose4JExpectedClaimValidator(
            final String claim, final String[] value) {

        return new Validator() {
            @Override
            public String validate(JwtContext jwtContext) throws MalformedClaimException {
                //
                if (jwtContext.getJwtClaims().hasClaim(claim)) {
                    if (jwtContext.getJwtClaims().isClaimValueOfType(claim, List.class)) {
                        //
                        List<String> claimValue = (List<String>) jwtContext.getJwtClaims().getClaimValue(claim);
                        boolean containsScopes = claimValue.containsAll(Arrays.asList(value));
                        //
                        return containsScopes
                                ? null
                                : String.format(
                                "claim %s value: %s does not match value present in JWT: %s",
                                claim, Arrays.toString(value), Arrays.toString(claimValue.toArray()));
                    } else {
                        return String.format(
                                "claim: %s value: %s is not of type array!",
                                claim, jwtContext.getJwtClaims().getClaimValue(claim));
                    }

                } else {
                    return String.format("JWT does not have %s claim", claim);
                }
            }
        };
    }

}
