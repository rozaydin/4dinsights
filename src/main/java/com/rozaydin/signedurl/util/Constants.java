package com.rozaydin.signedurl.util;

public interface Constants {

    String SCOPE_CLAIM = "scope";
    // Scopes
    String PASSWORD_RESET_SCOPE = "password_reset";

}