package com.rozaydin.signedurl.util;

import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.NumericDate;
import org.jose4j.lang.JoseException;

import java.security.PrivateKey;
import java.util.UUID;

public class JwtUtil {

    /**
     * Creates an IAT token with provided parameters
     * Signs the token with provided privateKey using
     * RSA_SHA256
     *
     * @param issuer                   iss claim value
     * @param subject                  sub claim value
     * @param validityPeriodInSeconds  how many seconds token will remain valid
     * @param jti                      jwt token id
     * @param scope                    scope claim value
     * @param keyId                    JOSE header keyId value
     * @param privateKey               privateKey to sign JWS
     * @return String representation of JWS
     * @throws JoseException if an exception occurs while encoding the JWS to while signing it
     */
    public static String createJwt(String issuer, String subject, long validityPeriodInSeconds, String jti, String[] scope,
                                   String keyId, PrivateKey privateKey) throws JoseException
    {
        // create JWS
        JsonWebSignature jws = new JsonWebSignature();
        jws.setHeader("alg", AlgorithmIdentifiers.RSA_USING_SHA256);
        jws.setHeader("typ", "JWT");
        jws.setHeader("kid", keyId);
        // create body
        JwtClaims claims = new JwtClaims();
        claims.setIssuer(issuer);
        claims.setSubject(subject);
        //
        NumericDate iat = NumericDate.now();
        NumericDate exp = NumericDate.now();
        exp.addSeconds(validityPeriodInSeconds);
        //
        claims.setIssuedAt(iat);
        claims.setNotBefore(iat);
        claims.setExpirationTime(exp);
        claims.setJwtId(jti);
        claims.setClaim("scope", scope);
        // set jws payload
        jws.setPayload(claims.toJson());
        // sign with private key
        jws.setKey(privateKey);
        jws.sign();
        //
        return jws.getCompactSerialization();
    }

    public static String createJti() {
        return UUID.randomUUID().toString();
    }

}
