package com.rozaydin.signedurl.service;

import com.rozaydin.signedurl.entity.UserEntity;
import com.rozaydin.signedurl.exceptions.UserNotFoundException;
import com.rozaydin.signedurl.repository.UserRepository;
import com.rozaydin.signedurl.util.JwtUtil;
import lombok.extern.slf4j.Slf4j;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtContext;
import org.jose4j.lang.JoseException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.security.PrivateKey;
import java.util.Optional;

import static com.rozaydin.signedurl.util.Constants.PASSWORD_RESET_SCOPE;

@Slf4j
@Service
public class JwtService {

    private final PrivateKey privateKey;
    private final String issuer;
    private final String keyId;
    private final long jwtValidityPeriod;
    private final JwtConsumer jwtConsumer;
    private final UserRepository userRepository;

    public JwtService(PrivateKey privateKey,
                      @Value("${jwt.issuer}") String issuer,
                      @Value("${security.keystore.keypair.alias}") String keyId,
                      @Value("${jwt.validityPeriod}") long jwtValidityPeriod,
                      JwtConsumer jwtConsumer,
                      UserRepository userRepository) {
        this.privateKey = privateKey;
        this.issuer = issuer;
        this.keyId = keyId;
        this.jwtValidityPeriod = jwtValidityPeriod;
        this.jwtConsumer = jwtConsumer;
        this.userRepository = userRepository;
    }


    public String createJwt(long userId, String jti) throws JoseException {
        return JwtUtil.createJwt(issuer, String.valueOf(userId), jwtValidityPeriod, jti, new String[]{PASSWORD_RESET_SCOPE}, keyId, privateKey);
    }

    public boolean validateJwt(Long userId, String jwt) {

        try {

            log.info("validating jwt");
            final JwtContext jwtContext = jwtConsumer.process(jwt);

            // userId check
            if (userId != Long.valueOf(jwtContext.getJwtClaims().getSubject())) {
                log.info("provided userId: {} does not match with jwt subject: {}, validation failed!", userId, jwtContext.getJwtClaims().getSubject());
                return false;
            }

            // user existence check
            Optional<UserEntity> user = userRepository.findById(userId);
            if (!user.isPresent()) {
                throw new UserNotFoundException("No user with id: " + userId + " exists! validation failed");
            }

            // jti equality check
            final String jti = jwtContext.getJwtClaims().getJwtId();
            if (!user.get().getJti().equals(jti)) {
                log.info("Jwt jti: {} does not match with the user jti: {} in DB! validation failed!", jti, user.get().getJti());
                return false;
            }

            // all conditions pass
            return true;

        } catch (Exception exc) {
            log.warn("Failed to validate jwt: {} due to exc!", jwt, exc);
            return false;
        }
    }

}
