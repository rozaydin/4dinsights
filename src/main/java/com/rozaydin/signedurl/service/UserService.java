package com.rozaydin.signedurl.service;

import com.rozaydin.signedurl.entity.UserEntity;
import com.rozaydin.signedurl.exceptions.UserNotFoundException;
import com.rozaydin.signedurl.repository.UserRepository;
import com.rozaydin.signedurl.util.JwtUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@Service
public class UserService {

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public String updateJti(final Long userId) {

        Optional<UserEntity> user = userRepository.findById(userId);

        if (!user.isPresent()) {
            throw new UserNotFoundException("User with id: " + userId + " does not exists!");
        }

        final String jti = JwtUtil.createJti();
        user.get().setJti(jti);
        userRepository.save(user.get());
        return jti;
    }

    public void resetPassword(final long userId, final String newPassword) {

        Optional<UserEntity> user = userRepository.findById(userId);

        if (!user.isPresent()) {
            throw new UserNotFoundException("User with id: " + userId + " does not exists!");
        }

        // clear jti - this will invalidate token as well
        user.get().setJti("");
        user.get().setPassword(newPassword);
        userRepository.save(user.get());
    }

    public boolean login(final long userId, final String password) {

        Optional<UserEntity> user = userRepository.findById(userId);

        if (!user.isPresent()) {
            throw new UserNotFoundException("User with id: " + userId + " does not exists!");
        }

        // compare passwords
        System.out.println(user.get().getPassword());
        return user.get().getPassword().equals(password);

    }

}
