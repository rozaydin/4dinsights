package com.rozaydin.signedurl.controller;

import com.rozaydin.signedurl.exceptions.UserNotFoundException;
import com.rozaydin.signedurl.model.LoginRequest;
import com.rozaydin.signedurl.model.ResetPasswordRequest;

import com.rozaydin.signedurl.model.Response;

import static com.rozaydin.signedurl.model.Response.*;

import com.rozaydin.signedurl.service.JwtService;
import com.rozaydin.signedurl.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

@CrossOrigin
@Slf4j
@RestController
@RequestMapping("/api/v1/user")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;
    private final JwtService jwtService;

    // this rest method is written for demo purposes
    @PostMapping(path = "login", produces = "application/json")
    public ResponseEntity<Response> login(@RequestBody LoginRequest loginRequest) {
        log.info("Processing login request for userId: {}", loginRequest.getUserId());
        boolean loginResult = userService.login(loginRequest.getUserId(), loginRequest.getPassword());
        log.info("login request processing result: {} for userId: {}", loginResult, loginRequest.getUserId());
        return new ResponseEntity<>(createResponse(loginResult, ""), HttpStatus.OK);
    }

    @PutMapping(path = "{id}/requestResetPassword", produces = "application/json")
    public ResponseEntity<Response> requestResetPassword(@PathVariable("id") final Long userId) {
        try {
            final String jti = userService.updateJti(userId);
            final String jwt = jwtService.createJwt(userId, jti);
            return new ResponseEntity<>(createResponse(true, "request processed successfully", jwt), HttpStatus.OK);
        } catch (Exception exc) {
            log.warn("Failed to create reset password link due to exc: ", exc);
            return new ResponseEntity<>(createResponse(false, "Request Failed!"), HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping(path = "{id}/validateResetPasswordToken", produces = "application/json")
    public ResponseEntity<Response> validateResetPasswordToken(@PathVariable("id") final Long userId, @RequestParam("token") final String token) {
        return jwtService.validateJwt(userId, token) ?
                new ResponseEntity<>(createResponse(true, "validation success"), HttpStatus.OK) :
                new ResponseEntity<>(createResponse(false, "validation failed"), HttpStatus.BAD_REQUEST);

    }

    @PostMapping(path = "{id}/resetPassword", produces = "application/json")
    public ResponseEntity<Response> resetPassword(@PathVariable("id") final Long userId, @RequestBody ResetPasswordRequest resetPasswordRequest) {
        if (jwtService.validateJwt(userId, resetPasswordRequest.getToken())) {
            userService.resetPassword(userId, resetPasswordRequest.getPassword());
            return new ResponseEntity<>(createResponse(true, "password updated!"), HttpStatus.OK);
        }
        return new ResponseEntity<>(createResponse(true, "invalid credentials!"), HttpStatus.FORBIDDEN);
    }

    // exception handler

    @ExceptionHandler({UserNotFoundException.class})
    public ResponseEntity<Response> handleException(RuntimeException e, WebRequest request) {
        return new ResponseEntity<>(createResponse(false, "Request Failed!"), HttpStatus.BAD_REQUEST);
    }

}