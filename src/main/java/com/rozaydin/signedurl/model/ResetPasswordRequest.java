package com.rozaydin.signedurl.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class ResetPasswordRequest {
    private final String token;
    private final String password;
}
