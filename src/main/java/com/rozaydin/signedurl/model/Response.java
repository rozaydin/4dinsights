package com.rozaydin.signedurl.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class Response {

    private final boolean result;
    private final String message;
    private final String data;

    public static Response createResponse(boolean result, String message, String data) {
        return new Response(result, message, data);
    }

    public static Response createResponse(boolean result, String message) {
        return new Response(result, message, "");
    }

}
