package com.rozaydin.signedurl.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class LoginRequest {

    private final Long userId;
    private final String password;

}
